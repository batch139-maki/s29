const express = require("express");
const app = express();
const PORT = 4000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));


let users =[ 
	{
		username: "johndoe",
		password: "johndoe1234"
	},
	{
		username: "maki",
		password: "asdfg"
	},
	{
		username: "super",
		password: "wasd"
	}
];


app.get("/home", (req, res) => res.send(`Welcome to the home page!`));

app.get("/users", (req, res) => res.send(users));


app.delete("/delete-user", (req, res) => {

	let message = ""

	for(let i = 0; i < users.length; i++){

		if(req.body.username === users[i].username){
			users.splice(i,1)
			message = `User ${req.body.username} has been deleted`
		} else {
			message = `User does not exist.`
		}
	}
	console.log(users)
	res.send(message)
})

console.log(users)
app.listen(PORT, () => console.log(`Server running at port ${PORT}`))
